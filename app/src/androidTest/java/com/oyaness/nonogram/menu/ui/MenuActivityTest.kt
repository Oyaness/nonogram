package com.oyaness.nonogram.menu.ui

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.oyaness.nonogram.view.activity.MenuActivity
import nonogram.oyaness.nonogram.R
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * @author Oya
 * @date 12.02.19
 */

@RunWith(AndroidJUnit4::class)
class MenuActivityTest {

    @Rule
    @JvmField
    var activityRule = ActivityTestRule<MenuActivity>(MenuActivity::class.java)

    @Test
    fun setComponents() {

    }

    @Test
    fun btnAboutClicked() {
        onView(withId(R.id.btnAbout)).perform(click())
        onView(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                withId(R.id.root_about)))
                .check(matches(isDisplayed()))
    }

    @Test
    fun btnSettingsClicked() {
        onView(withId(R.id.btnSettings)).perform(click())
        onView(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                withId(R.id.root_settings)))
                .check(matches(isDisplayed()))
    }

    @Test
    fun btnAboutCloseClicked() {
        btnAboutClicked()
        onView(withId(R.id.btnAboutClose)).perform(click())
        onView(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                withId(R.id.root_menu)))
                .check(matches(isDisplayed()))
    }

    @Test
    fun btnSettingsCloseClicked() {
       btnSettingsClicked()
        onView(withId(R.id.btnSettingsClose)).perform(click())
        onView(allOf(
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE),
                withId(R.id.root_menu)))
                .check(matches(isDisplayed()))
    }

    @Test fun starsClicked(){
        btnAboutClicked()
        onView(withId(R.id.viewStars)).perform(click())
    }

    @Test
    fun playButtonTest() {
        onView(withId(R.id.btnPlay)).perform(click())
        onView(withId(R.id.btnPlay)).check(matches(isChecked()))

        onView(withId(R.id.btnAbout)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnSettings)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnRegular)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnTimer)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnEasy)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnMedium)).check(matches(not(isDisplayed())))
        onView(withId(R.id.btnHard)).check(matches(not(isDisplayed())))

        onView(withId(R.id.btnPlay)).perform(click())
        onView(withId(R.id.btnPlay)).check(matches(not(isChecked())))

        onView(withId(R.id.btnAbout)).check(matches(isDisplayed()))
        onView(withId(R.id.btnSettings)).check(matches(isDisplayed()))
        onView(withId(R.id.btnRegular)).check(matches(isDisplayed()))
        onView(withId(R.id.btnTimer)).check(matches(isDisplayed()))
        onView(withId(R.id.btnEasy)).check(matches(isDisplayed()))
        onView(withId(R.id.btnMedium)).check(matches(isDisplayed()))
        onView(withId(R.id.btnHard)).check(matches(isDisplayed()))
    }

}