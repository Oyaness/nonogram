package com.oyaness.nonogram.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.oyaness.nonogram.data.DataStore
import com.oyaness.nonogram.data.DataStoreImpl
import com.oyaness.nonogram.viewmodel.GameViewModel
import com.oyaness.nonogram.viewmodel.MenuViewModel
import com.oyaness.nonogram.utils.ResourceManager
import com.oyaness.nonogram.utils.SharedPreferencesManager
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * @author Oya
 * @date 26.02.19
 */

val appModule = module(override = true) {
    single { GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create() }
    single { SharedPreferencesManager(androidContext(), get()) }
    single { ResourceManager(androidContext()) }

    factory<DataStore> { DataStoreImpl(get()) }

    viewModel { MenuViewModel(get(), get()) }
    viewModel { GameViewModel(get()) }
}