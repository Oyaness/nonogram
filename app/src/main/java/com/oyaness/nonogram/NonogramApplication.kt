package com.oyaness.nonogram

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import com.oyaness.nonogram.di.appModule
import org.koin.android.ext.android.startKoin


class NonogramApplication : MultiDexApplication() {

    companion object {
        operator fun get(context: Context): NonogramApplication {
            return context.applicationContext as NonogramApplication
        }
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        startKoin(this, listOf(appModule))
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }
}