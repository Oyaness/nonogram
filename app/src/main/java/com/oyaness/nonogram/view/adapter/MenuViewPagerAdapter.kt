package com.oyaness.nonogram.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.oyaness.nonogram.base.BaseFragment

/**
 * @author Oya
 * @date 11.01.19
 */

class MenuViewPagerAdapter(fragmentManager: FragmentManager, private val pages: List<BaseFragment>) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment = pages[position]

    override fun getCount(): Int = pages.size
}