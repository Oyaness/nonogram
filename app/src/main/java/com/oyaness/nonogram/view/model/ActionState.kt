package com.oyaness.nonogram.view.model

import com.oyaness.nonogram.model.Type


/**
 * @author Oya
 * @date 28.02.19
 */
sealed class ActionState {
    object Pause : ActionState()
    object Resume : ActionState()
    class Restart(val gameType: Type) : ActionState()
    object Win : ActionState()
    object Back : ActionState()
}