package com.oyaness.nonogram.view.model

import com.oyaness.nonogram.model.Game


/**
 * @author Oya
 * @date 28.02.19
 */


sealed class InitialState {
    class Init(val game: Game, val lblDifficulty: String) : InitialState()
    class Error(val errorMessage: String) : InitialState()
}
