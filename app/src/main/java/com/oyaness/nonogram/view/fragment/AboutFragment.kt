package com.oyaness.nonogram.view.fragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.oyaness.nonogram.view.model.MenuPage
import kotlinx.android.synthetic.main.menu_about_fragment.*
import nonogram.oyaness.nonogram.BuildConfig
import nonogram.oyaness.nonogram.R

/**
 * @author Oya
 * @date 11.01.19
 */

class AboutFragment : MenuFragment() {
    companion object {
        fun newInstance(): AboutFragment {
            val fragment = AboutFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    /** Listeners */
    /** Base */
    override fun getLayout(): Int = R.layout.menu_about_fragment

    override fun initView() {
        btnAboutClose.setOnClickListener { listener.onPageSwitch(MenuPage.Game) }
        viewStars.setOnClickListener {
            launchMarket()
        }
    }


    /** Helper Methods */
    private fun launchMarket() {
        val appPackageName = BuildConfig.APPLICATION_ID
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
        } catch (ex: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
        }
    }
}