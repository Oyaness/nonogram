package com.oyaness.nonogram.view.activity

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import com.oyaness.nonogram.base.BaseActivity
import com.oyaness.nonogram.view.adapter.MenuViewPagerAdapter
import com.oyaness.nonogram.view.model.MenuPage
import com.oyaness.nonogram.utils.MenuPageTransformer
import com.oyaness.nonogram.utils.PageSwitchListener
import com.oyaness.nonogram.view.fragment.AboutFragment
import com.oyaness.nonogram.view.fragment.GameFragment
import com.oyaness.nonogram.view.fragment.SettingsFragment
import com.oyaness.nonogram.viewmodel.MenuViewModel
import com.oyaness.nonogram.utils.Song
import kotlinx.android.synthetic.main.menu_activity.*
import nonogram.oyaness.nonogram.R
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @author Oya
 * @date 11.01.19
 */

class MenuActivity : BaseActivity(), ViewPager.OnPageChangeListener, PageSwitchListener {

    companion object {
        const val RESULT_ERROR = 2
    }

    private val viewModel: MenuViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.musicState.observe(this, Observer { musicEnabled ->
            if (musicEnabled == true) {
                startMusic(Song.Menu)
            } else {
                stopMusic()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_ERROR) {
            showSnackbar(getString(R.string.error_game_load))
        }
    }

    override fun onResume() {
        super.onResume()
        if (viewModel.isMusicEnabled()) {
            startMusic(Song.Menu)
        }
    }

    /** Listeners */
    /** Base */
    override fun setLayout(): Int = R.layout.menu_activity

    override fun setRootView(): ViewGroup = root

    override fun initView() {
        menuPager.addOnPageChangeListener(this)
        menuPager.setPageTransformer(false, MenuPageTransformer())
        menuPager.adapter = MenuViewPagerAdapter(supportFragmentManager, listOf(AboutFragment.newInstance(), GameFragment.newInstance(), SettingsFragment.newInstance()))
        menuPager.currentItem = 1
    }

    /** Paging */
    override fun onPageScrollStateChanged(p0: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        val imageWidth = imageView?.width ?: 0
        val pagerWidth = menuPager?.width ?: 0
        val adapterSize = menuPager?.adapter?.count ?: 0

        val x = viewModel.computeX(imageWidth, pagerWidth, adapterSize, position, positionOffsetPixels)
        scrollView?.scrollTo(x + imageWidth / 3, 0)
    }

    override fun onPageSelected(p0: Int) {
    }

    override fun onPageSwitch(page: MenuPage) {
        menuPager?.currentItem = page.index
    }
}