package com.oyaness.nonogram.view.model


/**
 * @author Oya
 * @date 25.02.19
 */

enum class MenuPage(val index: Int) {
    About(0),
    Game(1),
    Settings(2)
}