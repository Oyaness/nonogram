package com.oyaness.nonogram.view.fragment

import android.animation.Animator
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.oyaness.nonogram.view.activity.GameActivity
import com.oyaness.nonogram.view.model.MenuPage
import com.oyaness.nonogram.viewmodel.MenuViewModel
import com.oyaness.nonogram.model.Difficulty
import com.oyaness.nonogram.model.Type
import kotlinx.android.synthetic.main.menu_game_fragment.*
import nonogram.oyaness.nonogram.R
import org.koin.android.viewmodel.ext.android.sharedViewModel

/**
 * @author Oya
 * @date 11.01.19
 */

class GameFragment : MenuFragment() {

    private val statePlay = intArrayOf(R.attr.state_menu_play, -R.attr.state_menu_back)
    private val stateBack = intArrayOf(-R.attr.state_menu_play, R.attr.state_menu_back)

    companion object {
        fun newInstance(): GameFragment {
            val fragment = GameFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    private var showPlayMenuFlag = false
    private val viewModel: MenuViewModel by sharedViewModel()

    private val typeObserver = Observer<Type> {
        cloudImage.setImageState(it?.state, true)
        when (it) {
            Type.Regular -> {
                btnRegular.isSelected = true
                btnTimer.isSelected = false
            }
            Type.Timer -> {
                btnRegular.isSelected = false
                btnTimer.isSelected = true
            }
        }
    }

    private val difficultyObserver = Observer<Difficulty> {
        treeImage.setImageState(it?.state, true)
        when (it) {
            Difficulty.Easy -> {
                btnEasy.isSelected = true
                btnMedium.isSelected = false
                btnHard.isSelected = false
            }
            Difficulty.Medium -> {
                btnEasy.isSelected = false
                btnMedium.isSelected = true
                btnHard.isSelected = false
            }
            Difficulty.Hard -> {
                btnEasy.isSelected = false
                btnMedium.isSelected = false
                btnHard.isSelected = true
            }
        }
    }


    override fun onPause() {
        super.onPause()
        showPlayMenuFlag = false
        updatePlayView(false)
    }


    /** Listeners */
    /** Base */
    override fun getLayout(): Int = R.layout.menu_game_fragment

    override fun initView() {
        btnResume.setOnClickListener { startGame(false) }
        btnNew.setOnClickListener { startGame(true) }
        btnAbout.setOnClickListener { listener.onPageSwitch(MenuPage.About) }
        btnSettings.setOnClickListener { listener.onPageSwitch(MenuPage.Settings) }
        btnPlay.setOnClickListener { playClicked() }
        btnEasy.setOnClickListener { viewModel.setDifficulty(Difficulty.Easy) }
        btnMedium.setOnClickListener { viewModel.setDifficulty(Difficulty.Medium) }
        btnHard.setOnClickListener { viewModel.setDifficulty(Difficulty.Hard) }
        btnRegular.setOnClickListener { viewModel.setType(Type.Regular) }
        btnTimer.setOnClickListener { viewModel.setType(Type.Timer) }

        viewModel.gameType.observe(this, typeObserver)
        viewModel.gameDifficulty.observe(this, difficultyObserver)
    }

    /** Helper Methods */
    private fun playClicked() {
        if (viewModel.hasExistingGame()) {
            showPlayMenuFlag = !showPlayMenuFlag
            updatePlayView(showPlayMenuFlag)
        } else {
            startGame(true)

        }
    }

    private fun startGame(isNew: Boolean) {
        context?.let {
            if (isNew) {
                viewModel.createGame()
            }
            startActivityForResult(GameActivity.getIntent(it), 2)
        }
    }

    private fun updatePlayView(show: Boolean) {
        if (show) {
            btnPlay.setImageState(stateBack, true)
            btnAbout.visibility = View.GONE
            btnSettings.visibility = View.GONE
            btnRegular.visibility = View.GONE
            btnTimer.visibility = View.GONE
            btnEasy.visibility = View.GONE
            btnMedium.visibility = View.GONE
            btnHard.visibility = View.GONE

            viewPlay.visibility = View.VISIBLE
            viewPlay.translationY = -(viewPlay.height / 2).toFloat()
            viewPlay.alpha = 0.0f
            viewPlay.animate()
                    .translationY(0f)
                    .alpha(1.0f)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {
                        }

                        override fun onAnimationCancel(animation: Animator?) {
                        }

                        override fun onAnimationStart(animation: Animator?) {
                            btnNew.isEnabled = false
                            btnResume.isEnabled = false
                        }

                        override fun onAnimationEnd(animation: Animator?) {
                            btnNew.isEnabled = true
                            btnResume.isEnabled = true
                        }
                    })
        } else {
            btnPlay.setImageState(statePlay, true)
            btnAbout.visibility = View.VISIBLE
            btnSettings.visibility = View.VISIBLE
            btnRegular.visibility = View.VISIBLE
            btnTimer.visibility = View.VISIBLE
            btnEasy.visibility = View.VISIBLE
            btnMedium.visibility = View.VISIBLE
            btnHard.visibility = View.VISIBLE

            viewPlay.animate()
                    .translationY(-(viewPlay.height / 2).toFloat())
                    .alpha(0.0f)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {
                        }

                        override fun onAnimationCancel(animation: Animator?) {
                        }

                        override fun onAnimationStart(animation: Animator?) {
                            btnNew.isEnabled = false
                            btnResume.isEnabled = false
                        }

                        override fun onAnimationEnd(animation: Animator?) {
                        }
                    })
        }
    }
}