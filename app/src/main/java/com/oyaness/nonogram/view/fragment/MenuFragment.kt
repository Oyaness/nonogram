package com.oyaness.nonogram.view.fragment

import android.content.Context
import com.oyaness.nonogram.base.BaseFragment
import com.oyaness.nonogram.utils.PageSwitchListener


/**
 * @author Oya
 * @date 25.02.19
 */

abstract class MenuFragment : BaseFragment() {
    protected lateinit var listener: PageSwitchListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is PageSwitchListener) {
            listener = context
        }
    }
}