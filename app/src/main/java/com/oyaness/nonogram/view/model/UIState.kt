package com.oyaness.nonogram.view.model

import nonogram.oyaness.nonogram.R


/**
 * @author Oya
 * @date 15.02.19
 */

enum class UIState(val state: IntArray) {
    Pause(intArrayOf(R.attr.state_play, -R.attr.state_pause, -R.attr.state_win, -R.attr.state_restart)),
    Ongoing(intArrayOf(-R.attr.state_play, R.attr.state_pause, -R.attr.state_win, -R.attr.state_restart)),
    Lose(intArrayOf(-R.attr.state_play, -R.attr.state_pause, -R.attr.state_win, R.attr.state_restart)),
    Win(intArrayOf(-R.attr.state_play, -R.attr.state_pause, R.attr.state_win, -R.attr.state_restart))
}