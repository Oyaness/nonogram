package com.oyaness.nonogram.view.model


/**
 * @author Oya
 * @date 28.02.19
 */

sealed class WinState {
    class Win(val score: Int, val hintCount: Int, val mistakeCount: Int) : WinState()
    class Mistake(val mistakeCount: Int) : WinState()
    class Lose(val hintCount: Int, val mistakeCount: Int) : WinState()
}