package com.oyaness.nonogram.view.activity

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintSet
import android.support.constraint.ConstraintSet.INVISIBLE
import android.transition.TransitionManager
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.oyaness.nonogram.base.BaseActivity
import com.oyaness.nonogram.model.Game
import com.oyaness.nonogram.utils.Song
import com.oyaness.nonogram.utils.view.GameGrid
import com.oyaness.nonogram.utils.view.HintButton
import com.oyaness.nonogram.utils.view.TimerTextView
import com.oyaness.nonogram.view.model.ActionState
import com.oyaness.nonogram.view.model.InitialState
import com.oyaness.nonogram.view.model.UIState
import com.oyaness.nonogram.view.model.WinState
import com.oyaness.nonogram.viewmodel.GameViewModel
import kotlinx.android.synthetic.main.activity_game.*
import nonogram.oyaness.nonogram.R
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * @author Oya
 * @date 11.01.19
 */

class GameActivity : BaseActivity(), TimerTextView.TimerListener, HintButton.HintClickedListener, GameGrid.GridClickListener {

    companion object {

        fun getIntent(context: Context): Intent {
            return Intent(context, GameActivity::class.java)
        }
    }

    private val viewModel: GameViewModel by viewModel()
    private val constraintOngoing = ConstraintSet()
    private val constraintPause = ConstraintSet()
    private val constraintWin = ConstraintSet()
    private val constraintLose = ConstraintSet()
    override fun onResume() {
        super.onResume()
        if (viewModel.isMusicEnabled()) {
            startMusic(Song.Game)
        }
    }

    override fun onBackPressed() {
        viewModel.onBackClicked(tvTimer.getTime())
    }

    /** Listeners */
    /** Base */
    override fun setLayout(): Int = R.layout.activity_game

    override fun setRootView(): ViewGroup = viewRoot

    override fun initView() {
        viewModel.initState.observe(this, initialStateObserver)
        viewModel.winState.observe(this, winStateObserver)
        viewModel.uiState.observe(this, uiStateObserver)
        viewModel.actionState.observe(this, actionStateObserver)
        initConstraints()

        viewModel.loadGame()

        btnStateChange.setOnClickListener { viewModel.onStateChanged() }

        btnCheck.setOnClickListener { viewModel.onGameCheckClicked() }
        btnRetry.setOnClickListener { viewModel.onRestartClicked() }
        btnRestart.setOnClickListener { viewModel.onRestartClicked() }
        btnSaveQuit.setOnClickListener { viewModel.quitGame(tvTimer.getTime()) }
        btnLoseQuit.setOnClickListener { onBackPressed() }
        btnMenu.setOnClickListener { onBackPressed() }
    }

    override fun onTimerComplete() {
        viewModel.onGameLost()
    }

    override fun onHintClicked() {
        val hint = viewModel.onHintClicked()
        hint?.let {
            grid.updateCellWith(it.x, it.y, it.checked)
        }
    }

    override fun hintNotLoaded() {
        doBadAnimation(btnHint)
    }

    override fun onCellClicked(i: Int, j: Int, isChecked: Boolean) {
        viewModel.updateGuessedCell(i, j, isChecked)
    }

    /** Helper Methods */
    private fun doBadAnimation(view: View) {
        val animShake = AnimationUtils.loadAnimation(this@GameActivity, R.anim.shake)
        view.startAnimation(animShake)
    }

    private val initialStateObserver = Observer<InitialState> {
        when (it) {
            is InitialState.Error -> processBack(it.errorMessage)
            is InitialState.Init -> setupGame(it.game, it.lblDifficulty)
        }
    }

    private val winStateObserver = Observer<WinState> {
        when (it) {
            is WinState.Lose -> tvTimer.updateTime(0)
            is WinState.Win -> {
                tvTimer.stop()
                tvScore.text = it.score.toString()
            }
            is WinState.Mistake -> {
                doBadAnimation(btnCheck)
                tvTimer.dealMistake(it.mistakeCount)
            }
        }
    }

    private val uiStateObserver = Observer<UIState> {
        TransitionManager.beginDelayedTransition(viewRoot)
        when (it) {
            UIState.Pause -> {
                btnStateChange.setImageState(it.state, true)
                constraintPause.applyTo(viewRoot)
            }
            UIState.Ongoing -> {
                btnStateChange.setImageState(it.state, true)

                constraintOngoing.applyTo(viewRoot)
                if (viewModel.areHintsEnabled()) {
                    btnHint.setOnHintClickedListener(this)
                    btnHint.visibility = View.VISIBLE
                } else {
                    btnHint.visibility = View.INVISIBLE
                }
            }
            UIState.Lose -> {
                btnStateChange.setImageState(it.state, true)
                constraintLose.applyTo(viewRoot)
            }
            UIState.Win -> {
                viewModel.calculateScore(tvTimer.getTime())
                btnStateChange.setImageState(it.state, true)
                constraintWin.applyTo(viewRoot)
            }
        }
    }

    private val actionStateObserver = Observer<ActionState> {
        when (it) {
            ActionState.Pause -> tvTimer.stop()
            ActionState.Resume -> tvTimer.resume()
            is ActionState.Restart -> {
                tvTimer.start(it.gameType, 0)
                grid.reset()
            }
            ActionState.Back -> processBack()
            ActionState.Win -> tvTimer.stop()
        }
    }

    private fun setupGame(game: Game, lblDifficulty: String) {
        tvDifficulty.text = lblDifficulty
        tvTimer.start(game.gameType, game.buffer)
        tvTimer.listener = this
        grid.setGridListener(this)
        grid.setupGrid(game)
    }

    private fun processBack(errorMessage: String? = null) {
        if (errorMessage == null) {
            finish()
        } else {
            setResult(MenuActivity.RESULT_ERROR)
            showSnackbar(errorMessage) {
                finish()
            }
        }
    }

    private fun initConstraints() {
        constraintOngoing.clone(viewRoot)

        constraintPause.clone(viewRoot)
        constraintPause.setVisibility(btnCheck.id, INVISIBLE)
        constraintPause.setVisibility(grid.id, INVISIBLE)
        constraintPause.connect(R.id.btnStateChange, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, resources.getDimension(R.dimen.big_margin).toInt())
        constraintPause.connect(viewPause.id, ConstraintSet.TOP, tvTimer.id, ConstraintSet.BOTTOM, resources.getDimension(R.dimen.small_margin).toInt())

        constraintLose.clone(viewRoot)
        constraintLose.setVisibility(btnCheck.id, INVISIBLE)
        constraintLose.setVisibility(grid.id, INVISIBLE)
        constraintLose.connect(R.id.btnStateChange, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, resources.getDimension(R.dimen.big_margin).toInt())
        constraintLose.connect(viewLose.id, ConstraintSet.TOP, tvTimer.id, ConstraintSet.BOTTOM, resources.getDimension(R.dimen.small_margin).toInt())

        constraintWin.clone(viewRoot)
        constraintWin.setVisibility(btnCheck.id, INVISIBLE)
        constraintWin.setVisibility(grid.id, INVISIBLE)
        constraintWin.connect(R.id.btnStateChange, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP, resources.getDimension(R.dimen.big_margin).toInt())
        constraintWin.connect(viewWin.id, ConstraintSet.TOP, tvTimer.id, ConstraintSet.BOTTOM, resources.getDimension(R.dimen.small_margin).toInt())
    }
}