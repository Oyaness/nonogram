package com.oyaness.nonogram.view.fragment

import android.os.Bundle
import com.oyaness.nonogram.view.model.MenuPage
import com.oyaness.nonogram.viewmodel.MenuViewModel
import kotlinx.android.synthetic.main.menu_settings_fragment.*
import nonogram.oyaness.nonogram.R
import org.koin.android.viewmodel.ext.android.sharedViewModel

/**
 * @author Oya
 * @date 11.01.19
 */

class SettingsFragment : MenuFragment() {

    companion object {
        fun newInstance(): SettingsFragment {
            val fragment = SettingsFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    private val viewModel: MenuViewModel by sharedViewModel()

    /** Listeners */
    /** Base */
    override fun getLayout(): Int = R.layout.menu_settings_fragment

    override fun initView() {
        btnSettingsClose.setOnClickListener { listener.onPageSwitch(MenuPage.Game) }
        cbMistake.isChecked = viewModel.mistakeCountFlag
        cbHint.isChecked = viewModel.enableHintsFlag
        cbHintCount.isChecked = viewModel.hintsCountShownFlag
        cbMusic.isChecked = viewModel.musicFlag
        cbSound.isChecked = viewModel.soundFlag

        cbMistake.setOnCheckedChangeListener { _, isChecked ->
            viewModel.mistakeCountFlag = isChecked
        }
        cbHint.setOnCheckedChangeListener { _, isChecked ->
            viewModel.enableHintsFlag = isChecked
        }
        cbHintCount.setOnCheckedChangeListener { _, isChecked ->
            viewModel.hintsCountShownFlag = isChecked
        }
        cbMusic.setOnCheckedChangeListener { _, isChecked ->
            viewModel.musicFlag = isChecked
        }
        cbSound.setOnCheckedChangeListener { _, isChecked ->
            viewModel.soundFlag = isChecked
        }
    }
}