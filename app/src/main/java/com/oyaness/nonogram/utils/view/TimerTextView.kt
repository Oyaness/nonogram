package com.oyaness.nonogram.utils.view

import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.SystemClock
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.oyaness.nonogram.model.Type
import nonogram.oyaness.nonogram.R

@Suppress("PrivatePropertyName", "DEPRECATION")

/** @author Oya */

class TimerTextView : AppCompatTextView {

    interface TimerListener {
        fun onTimerComplete()
    }

    private val MISTAKE: Long = 5000
    private val TIME: Long = 1000 * 60 * 5 //5 min

    var listener: TimerListener? = null

    private var type: Type = Type.Regular
    private var countDownTime: Long = 0
    private val timerHandler = Handler()
    private var timeInMilliseconds = 0L
    private var startTime = 0L
    private var timeSwapBuff = 0L
    private var isStarted = false

    private val updateTimer = object : Runnable {
        override fun run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime
            var updatedTime: Long = timeSwapBuff + timeInMilliseconds
            if (type == Type.Timer) {
                updatedTime = countDownTime - updatedTime
            }
            if (updatedTime < 0) {
                stopTimer()
                listener?.onTimerComplete()
                return
            }
            updateTime(updatedTime)
            timerHandler.postDelayed(this, 0)
        }
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        processAttrs(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        processAttrs(attrs)
    }

    fun start(type: Type, buffer: Long = 0) {
        if (!isStarted) {
            isStarted = true
            this.type = type
            if (type == Type.Timer) {
                this.countDownTime = TIME
            }
            timeSwapBuff = buffer
            startTimer()
        }
    }

    fun stop() {
        if (isStarted) {
            timeSwapBuff += timeInMilliseconds
            stopTimer()
        }
    }

    fun resume() {
        start(type, timeSwapBuff)
    }

    fun getTime(): Long {
        return timeSwapBuff
    }

    fun dealMistake(mistakeCount: Int) {
        timeSwapBuff += MISTAKE * mistakeCount
    }

    private fun processAttrs(attrs: AttributeSet) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.TimerTextView)
        type = if (a.getInt(R.styleable.TimerTextView_countType, 0) == 0) {
            Type.Regular
        } else {
            Type.Timer
        }
        countDownTime = a.getInt(R.styleable.TimerTextView_countDownTime, 0).toLong()
        a.recycle()

        if (Build.VERSION.SDK_INT < 23) {
            setTextAppearance(context, R.style.TitleSlimTextLight)

        } else {
            setTextAppearance(R.style.TitleSlimTextLight)
        }
//        textSize = resources.getDimension(R.dimen.timer_text)
//        setTextColor(ContextCompat.getColor(context, R.color.timer_text))
//        typeface = Typeface.create("sans-serif", Typeface.NORMAL)
    }

    private fun stopTimer() {
        isStarted = false
        timerHandler.removeCallbacks(updateTimer)
    }

    private fun startTimer() {
        startTime = SystemClock.uptimeMillis()
        timerHandler.postDelayed(updateTimer, 0)
    }

    fun updateTime(time: Long) {
        var seconds = (time / 1000).toInt()
        val minutes = seconds / 60
        seconds %= 60
        this.text = String.format("%d:%02d", minutes, seconds)
    }
}