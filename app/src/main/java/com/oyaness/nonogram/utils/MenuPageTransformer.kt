package com.oyaness.nonogram.utils

import android.support.v4.view.ViewPager
import android.view.View
import nonogram.oyaness.nonogram.R

/**
 * @author Oya
 * @date 11.01.19
 */

class MenuPageTransformer : ViewPager.PageTransformer {
    override fun transformPage(view: View, position: Float) {
        val pageWidth = view.width
        val absPosition = Math.abs(position)

        if (position <= 1 && position >= -1) {
            //Game Fragment
            val btnPlay = view.findViewById<View>(R.id.btnPlay)
            btnPlay?.let {
                it.scaleX = 1.0f - absPosition * 2
                it.scaleY = 1.0f - absPosition * 2
                it.alpha = 1.0f - absPosition * 2
            }

            val btnAbout = view.findViewById<View>(R.id.btnAbout)
            btnAbout?.let {
                it.scaleX = 1.0f - absPosition * 2
                it.scaleY = 1.0f - absPosition * 2
                it.alpha = 1.0f - absPosition * 2
            }

            val btnSettings = view.findViewById<View>(R.id.btnSettings)
            btnSettings?.let {
                it.scaleX = 1.0f - absPosition * 2
                it.scaleY = 1.0f - absPosition * 2
                it.alpha = 1.0f - absPosition * 2
            }

            val viewButtons = view.findViewById<View>(R.id.viewDataButtons)
            viewButtons?.let {
                it.translationY = pageWidth * absPosition
            }

            //About Fragment
            val btnAboutClose = view.findViewById<View>(R.id.btnAboutClose)
            btnAboutClose?.let {
                it.scaleX = 1.0f - absPosition * 2
                it.scaleY = 1.0f - absPosition * 2
                it.alpha = 1.0f - absPosition * 2
            }

            //Settings Fragment
            val btnSettingsClose = view.findViewById<View>(R.id.btnSettingsClose)
            btnSettingsClose?.let {
                it.scaleX = 1.0f - absPosition * 2
                it.scaleY = 1.0f - absPosition * 2
                it.alpha = 1.0f - absPosition * 2
            }


//                view.findViewById<View>(R.id.tv_app_subtitle).translationX = -(pageWidth * position)
//                view.findViewById<View>(R.id.userView).translationX = pageWidth * position
        }
    }
}