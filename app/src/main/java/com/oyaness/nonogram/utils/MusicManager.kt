package com.oyaness.nonogram.utils

import android.content.Context
import android.media.MediaPlayer
import android.support.annotation.RawRes
import nonogram.oyaness.nonogram.R


/**
 * @author Oya
 * @date 26.03.19
 */

enum class Song(@RawRes val source: Int) {
    Menu(R.raw.menu),
    Game(R.raw.game)
}

class MusicManager : MediaPlayer.OnErrorListener {

    private var player: MediaPlayer? = null
    private var length = 0

    fun start(context: Context, song: Song) {
        if (player == null) {
            player = MediaPlayer.create(context, song.source)
            player?.setOnErrorListener(this)
            player?.isLooping = true
            player?.setVolume(0.1f, 0.1f)
            player?.start()
        }
    }

    fun stop() {
        player?.stop()
        player?.release()
        player = null
    }

    fun pause() {
        player?.let {
            if (it.isPlaying) {
                it.pause()
                length = it.currentPosition

            }
        }
    }

    fun resume() {
        player?.let {
            if (!it.isPlaying) {
                it.seekTo(length)
                it.start()
            }
        }
    }

    /** Listeners */
    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        stop()
        return false
    }
}