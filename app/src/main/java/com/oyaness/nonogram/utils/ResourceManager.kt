package com.oyaness.nonogram.utils

import android.content.Context
import android.support.annotation.DimenRes
import android.support.annotation.StringRes


/**
 * @author Oya
 * @date 27.02.19
 */

class ResourceManager(private val context: Context) {

    fun getString(@StringRes resId: Int): String {
        return context.getString(resId)
    }

    fun getDimension(@DimenRes resId: Int): Float {
        return context.resources.getDimension(resId)
    }

    fun getScreenHeight(): Int {
        return context.applicationContext.resources.displayMetrics.heightPixels
    }

    fun getScreenWidth(): Int {
        return context.applicationContext.resources.displayMetrics.widthPixels
    }
}
