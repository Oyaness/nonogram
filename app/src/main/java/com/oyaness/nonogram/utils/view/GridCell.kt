package com.oyaness.nonogram.utils.view

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.CheckBox
import nonogram.oyaness.nonogram.R

/** @author Oya */

class GridCell : CheckBox, View.OnClickListener {

    private var i: Int = -1
    private var j: Int = -1

    private var listener: CellClickListener? = null

    interface CellClickListener {
        fun onCellClicked(i: Int, j: Int, isChecked: Boolean)
    }

    constructor(i: Int, j: Int, context: Context?) : super(context) {
        this.i = i
        this.j = j
        tag = context?.getString(R.string.cell_tag, i, j)
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        buttonDrawable = ContextCompat.getDrawable(context, R.drawable.selector_cell)
        setOnClickListener(this)
    }


    fun setCellClickListener(listener: CellClickListener) {
        this.listener = listener
    }

    override fun onClick(v: View?) {
        listener?.onCellClicked(i, j, isChecked)
    }
}