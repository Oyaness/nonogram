package com.oyaness.nonogram.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.oyaness.nonogram.model.Difficulty
import com.oyaness.nonogram.model.Game
import com.oyaness.nonogram.model.Type
import nonogram.oyaness.nonogram.BuildConfig


/**
 * @author Oya
 * @date 25.02.19
 */

class SharedPreferencesManager(context: Context, private val gson: Gson) {

    companion object {
        private const val KEY_TYPE = "KEY_TYPE"
        private const val KEY_DIFFICULTY = "KEY_DIFFICULTY"
        private const val KEY_GAME = "KEY_GAME"

        /** Settings Flags */
        private const val KEY_MISTAKE_COUNT = "KEY_MISTAKE_COUNT"
        private const val KEY_ENABLE_HINTS = "KEY_ENABLE_HINTS"
        private const val KEY_HINTS_USED = "KEY_HINTS_USED"
        private const val KEY_MUSIC = "KEY_MUSIC"
        private const val KEY_SOUND = "KEY_SOUND"
    }

    private val preferences: SharedPreferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)

    var type: Type
        get() =
            try {
                val typeString = preferences.getString(KEY_TYPE, Type.Regular.name)
                        ?: Type.Regular.name
                Type.valueOf(typeString)
            } catch (exception: IllegalArgumentException) {
                Type.Regular
            }
        set(value) = preferences.edit().putString(KEY_TYPE, value.name).apply()

    var difficulty: Difficulty
        get() =
            try {
                val difficultyString = preferences.getString(KEY_DIFFICULTY, Difficulty.Medium.name)
                        ?: Difficulty.Medium.name
                Difficulty.valueOf(difficultyString)
            } catch (exception: IllegalArgumentException) {
                Difficulty.Medium
            }
        set(value) = preferences.edit().putString(KEY_DIFFICULTY, value.name).apply()


    var game: Game?
        get() {
            val json = preferences.getString(KEY_GAME, null)
            json?.let {
                return gson.fromJson(json, Game::class.java)
            }
            return null
        }
        set(value) {
            val preferenceEditor = preferences.edit()
            val json = gson.toJson(value)
            preferenceEditor.putString(KEY_GAME, json)
            preferenceEditor.apply()
        }

    fun clearGame() {
        val preferenceEditor = preferences.edit()
        preferenceEditor.remove(KEY_GAME)
        preferenceEditor.apply()
    }

    var mistakeCountFlag: Boolean
        get() = preferences.getBoolean(KEY_MISTAKE_COUNT, false)
        set(value) = preferences.edit().putBoolean(KEY_MISTAKE_COUNT, value).apply()

    var enableHintsFlag: Boolean
        get() = preferences.getBoolean(KEY_ENABLE_HINTS, true)
        set(value) = preferences.edit().putBoolean(KEY_ENABLE_HINTS, value).apply()

    var hintsUsedFlag: Boolean
        get() = preferences.getBoolean(KEY_HINTS_USED, false)
        set(value) = preferences.edit().putBoolean(KEY_HINTS_USED, value).apply()

    var musicFlag: Boolean
        get() = preferences.getBoolean(KEY_MUSIC, true)
        set(value) = preferences.edit().putBoolean(KEY_MUSIC, value).apply()

    var soundFlag: Boolean
        get() = preferences.getBoolean(KEY_SOUND, true)
        set(value) = preferences.edit().putBoolean(KEY_SOUND, value).apply()
}