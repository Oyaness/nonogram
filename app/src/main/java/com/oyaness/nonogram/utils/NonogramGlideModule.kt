package com.oyaness.nonogram.utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class NonogramGlideModule : AppGlideModule()