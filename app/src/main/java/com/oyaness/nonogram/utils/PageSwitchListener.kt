package com.oyaness.nonogram.utils

import com.oyaness.nonogram.view.model.MenuPage

/**
 * @author Oya
 * @date 11.01.19
 */

interface PageSwitchListener {
    fun onPageSwitch(page: MenuPage)
}