package com.oyaness.nonogram.utils.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.GridLayout
import com.oyaness.nonogram.model.Game
import nonogram.oyaness.nonogram.R

/**
 * @author Oya
 * @date 08.04.19
 */

class GameGrid : GridLayout, GridCell.CellClickListener {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    interface GridClickListener {
        fun onCellClicked(i: Int, j: Int, isChecked: Boolean)
    }

    private var listener: GridClickListener? = null

    fun setGridListener(listener: GridClickListener) {
        this.listener = listener
    }

    fun reset(){
        val count = childCount
        for (i in 0 until count) {
            val child = getChildAt(i)
            (child as? GridCell)?.isChecked = false
        }
    }

    fun setupGrid(game: Game) {
        columnCount = 1
        rowCount = game.grid.cells.rows + 1

        var cb: GridCell
        var tv: GridTextView
        addView(GridTextView(context), getParams(null))
        for (i in 0 until game.grid.cells.rows) {
            tv = GridTextView(context)
            tv.text = game.grid.rowNumbers[i]
            addView(tv, getParams(false))
        }
        for (j in 0 until game.grid.cells.cols) {
            tv = GridTextView(context)
            tv.text = game.grid.colNumbers[j]
            addView(tv, getParams(true))
            for (i in 0 until game.grid.cells.rows) {
                cb = GridCell(i, j, context)
                cb.setCellClickListener(this)
                cb.isChecked = game.guessedCells.grid[i][j]
                addView(cb, getParams(null))
            }
        }
    }

    fun updateCellWith(row: Int, col: Int, isChecked: Boolean) {
        val item = findViewWithTag(context.getString(R.string.cell_tag, row, col)) as? GridCell
        item?.isChecked = isChecked
    }

    /** Listeners */
    override fun onCellClicked(i: Int, j: Int, isChecked: Boolean) {
        listener?.onCellClicked(i, j, isChecked)
    }

    /** Helper Methods */
    private fun getParams(isRow: Boolean?): GridLayout.LayoutParams {
        val param = GridLayout.LayoutParams()
        param.height = GridLayout.LayoutParams.WRAP_CONTENT
        param.width = GridLayout.LayoutParams.WRAP_CONTENT
        param.rightMargin = 1
        param.topMargin = 1
        param.bottomMargin = 1
        param.leftMargin = 1

        isRow?.let {
            if (isRow) {
                param.setGravity(Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM)
            } else {
                param.setGravity(Gravity.CENTER_VERTICAL or Gravity.END)
            }
        }
        return param
    }
}