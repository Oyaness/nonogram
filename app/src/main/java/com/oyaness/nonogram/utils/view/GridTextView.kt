package com.oyaness.nonogram.utils.view

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.widget.TextView
import nonogram.oyaness.nonogram.R

/**
 * @author Oya
 * @date 11.07.18
 */

@Suppress("DEPRECATION")
class GridTextView : TextView {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        if (Build.VERSION.SDK_INT < 23) {
            setTextAppearance(context, R.style.CaptionTextDark)

        } else {
            setTextAppearance(R.style.CaptionTextDark)
        }
    }
}