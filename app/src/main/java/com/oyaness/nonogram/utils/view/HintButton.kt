package com.oyaness.nonogram.utils.view

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import nonogram.oyaness.nonogram.R


/**
 * @author Oya
 * @date 26.03.19
 */

private enum class HintState(val state: IntArray) {
    Empty(intArrayOf(R.attr.state_hint_empty, -R.attr.state_hint_full)),
    Full(intArrayOf(-R.attr.state_hint_empty, R.attr.state_hint_full))
}

class HintButton : ImageView, View.OnClickListener {

    private var state: HintState = HintState.Full

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    interface HintClickedListener {
        fun onHintClicked()
        fun hintNotLoaded()
    }

    private var listener: HintClickedListener? = null

    init {
        setOnClickListener(this)
        setImageResource(R.drawable.ani_hint)
        setImageState(state.state, true)
    }

    fun setOnHintClickedListener(l: HintClickedListener) {
        this.listener = l
    }

    override fun onClick(v: View?) {
        if (state == HintState.Full) {
            listener?.onHintClicked()
            changeState(HintState.Empty)
            loadHint()
        } else {
            listener?.hintNotLoaded()
        }
    }

    private fun loadHint() {
        val handler = Handler()
        handler.postDelayed({
            changeState(HintState.Full)
        }, 10000)
    }

    private fun changeState(state: HintState) {
        this.state = state
        setImageState(state.state, true)
    }
}