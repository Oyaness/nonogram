package com.oyaness.nonogram.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.oyaness.nonogram.base.BaseViewModel
import com.oyaness.nonogram.data.DataStore
import com.oyaness.nonogram.view.model.ActionState
import com.oyaness.nonogram.view.model.InitialState
import com.oyaness.nonogram.view.model.UIState
import com.oyaness.nonogram.view.model.WinState
import com.oyaness.nonogram.model.Hint
import com.oyaness.nonogram.model.Type
import java.util.*

/**
 * @author Oya
 * @date 13.02.19
 */

class GameViewModel(private val dataStore: DataStore) : BaseViewModel() {

    private val hintError: Double = 10.toDouble()
    private val mistakeError: Double = 10.toDouble()

    private val _initState: MutableLiveData<InitialState> = MutableLiveData()
    val initState: LiveData<InitialState>
        get() = _initState

    private val _winState: MutableLiveData<WinState> = MutableLiveData()
    val winState: LiveData<WinState>
        get() = _winState


    private val _uiState: MutableLiveData<UIState> = MutableLiveData()
    val uiState: LiveData<UIState>
        get() = _uiState

    private val _actionState: MutableLiveData<ActionState> = MutableLiveData()
    val actionState: LiveData<ActionState>
        get() = _actionState

    private var game = dataStore.getGame()
    private var hintCount: Int = 0
    private var mistakeCount: Int = 0

    init {
        _uiState.value = UIState.Ongoing
    }
    fun isMusicEnabled(): Boolean {
        return dataStore.getMusicFlag()
    }

    fun areHintsEnabled(): Boolean {
        return dataStore.getHintsEnabledFlag()
    }

    fun loadGame() {
        if (game == null) {
            _initState.value = InitialState.Error("Failed to load game")
        } else {
            _initState.value = InitialState.Init(game!!, game!!.grid.difficulty.name)
        }
    }

    fun updateGuessedCell(i: Int, j: Int, isChecked: Boolean) {
        game?.let {
            it.guessedCells.grid[i][j] = isChecked
        }
    }

    fun onStateChanged() {
        when (_uiState.value) {
            UIState.Pause -> {
                _actionState.value = ActionState.Resume
                _uiState.value = UIState.Ongoing

            }
            UIState.Ongoing, null -> {
                onPauseClicked()
            }
            UIState.Lose -> {
                onRestartClicked()
            }
            else -> {
            }
        }
    }

    fun onRestartClicked() {
        restartGame()
        _actionState.value = ActionState.Restart(game?.gameType ?: Type.Regular)
        _uiState.value = UIState.Ongoing
    }

    fun onBackClicked(buffer: Long) {
        if (_uiState.value == UIState.Ongoing || _uiState.value == null) {
            onPauseClicked()
        } else {
            quitGame(buffer)
            _actionState.value = ActionState.Back
        }
    }

    fun onHintClicked(): Hint? {
        hintCount++
        game?.let {
            val hint = getHint()
            hint?.let { h ->
                it.guessedCells.grid[h.x][h.y] = h.checked
                return h
            }
        }
        return null
    }

    fun onGameCheckClicked() {
        if (didWinGame()) {
            dataStore.clearGame()
            _actionState.value = ActionState.Win
            _uiState.value = UIState.Win
        } else {
            mistakeCount++
            if (game?.gameType == Type.Timer) {
                _winState.value = WinState.Mistake(mistakeCount)
            }
        }
    }

    fun onGameLost() {
        dataStore.clearGame()
        _uiState.value = UIState.Lose
        _winState.value = WinState.Lose(hintCount, mistakeCount)
    }

    fun quitGame(buffer: Long) {
        if (game != null && !isGameFinished()) {
            game!!.buffer = buffer
            dataStore.setGame(game!!)
        }
        _actionState.value = ActionState.Back
    }

    /** Helper Methods */
    private fun onPauseClicked() {
        _actionState.value = ActionState.Pause
        _uiState.value = UIState.Pause
    }

    private fun didWinGame(): Boolean {
        val game = game ?: return false

        for (i in 0 until game.grid.cells.rows) {
            for (j in 0 until game.grid.cells.cols) {
                if (game.guessedCells.grid[i][j] != game.grid.cells.grid[i][j]) {
                    return false
                }
            }
        }
        return true
    }

    private fun restartGame() {
        val rows = getCellRowCount()
        val cols = getCellColCount()

        game?.let {
            for (i in 0 until rows) {
                for (j in 0 until cols) {
                    it.guessedCells.grid[i][j] = false
                }
            }
        }
    }

    fun calculateScore(buffer: Long) {
        //TODO check calculation method
        //498333 8:18
        val score =  (10000000 / buffer + (hintCount * hintError) + Math.pow(mistakeError, mistakeCount.toDouble())).toInt()

        _winState.value = WinState.Win(score, hintCount, mistakeCount)
    }

    private fun getCellRowCount(): Int {
        return game?.grid?.cells?.rows ?: 0
    }

    private fun getCellColCount(): Int {
        return game?.grid?.cells?.cols ?: 0
    }

    private fun isGameFinished(): Boolean {
        return _uiState.value == UIState.Win || _uiState.value == UIState.Lose
    }

    private fun getHint(): Hint? {
        val game = game ?: return null
        return if (!didWinGame()) {
            val random = Random()
            var row: Int
            var col: Int
            do {
                row = random.nextInt(game.grid.cells.rows)
                col = random.nextInt(game.grid.cells.cols)
            } while (game.grid.cells.grid[row][col] == game.guessedCells.grid[row][col])
            Hint(row, col, game.grid.cells.grid[row][col])
        } else {
            null
        }
    }
}