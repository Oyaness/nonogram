package com.oyaness.nonogram.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.oyaness.nonogram.base.BaseViewModel
import com.oyaness.nonogram.data.DataStore
import com.oyaness.nonogram.model.Difficulty
import com.oyaness.nonogram.model.Game
import com.oyaness.nonogram.model.Type
import com.oyaness.nonogram.utils.ResourceManager
import nonogram.oyaness.nonogram.R

/**
 *
 * @author Oya
 * @date 13.02.19
 */

class MenuViewModel(private val dataStore: DataStore, private val resourceManager: ResourceManager) : BaseViewModel() {

    private val _gameType: MutableLiveData<Type> = MutableLiveData()
    val gameType: LiveData<Type>
        get() = _gameType

    private val _gameDifficulty: MutableLiveData<Difficulty> = MutableLiveData()
    val gameDifficulty: LiveData<Difficulty>
        get() = _gameDifficulty

    private val _musicState: MutableLiveData<Boolean> = MutableLiveData()
    val musicState: LiveData<Boolean>
        get() = _musicState

    init {
        _gameDifficulty.value = dataStore.getGameDifficulty()
        _gameType.value = dataStore.getGameType()
    }

    fun isMusicEnabled(): Boolean {
        return dataStore.getMusicFlag()
    }

    fun createGame() {
        val game = Game(_gameType.value ?: Type.Regular,
                _gameDifficulty.value ?: Difficulty.Medium,
                resourceManager.getScreenHeight(),
                resourceManager.getScreenWidth(),
                resourceManager.getDimension(R.dimen.cell_size),
                resourceManager.getDimension(R.dimen.cell_margin))
        dataStore.setGame(game)
    }

    fun hasExistingGame(): Boolean {
        return dataStore.getGame() != null
    }

    fun setDifficulty(difficulty: Difficulty) {
        _gameDifficulty.value = difficulty
        dataStore.setGameDifficulty(difficulty)
    }

    fun setType(type: Type) {
        _gameType.value = type
        dataStore.setGameType(type)
    }

    fun computeX(imageWidth: Int, pagerWidth: Int, adapterSize: Int, position: Int, positionOffsetPixels: Int): Int {
        val factor = ((imageWidth - pagerWidth) / 2) / (pagerWidth * adapterSize - 1).toFloat()
        return ((pagerWidth * position + positionOffsetPixels) * factor).toInt()
    }

    /** Settings Fragment */
    var mistakeCountFlag: Boolean
        get() = dataStore.getMistakeCountFlag()
        set(value) {
            dataStore.setMistakeCountFlag(value)
        }

    var enableHintsFlag: Boolean
        get() = dataStore.getHintsEnabledFlag()
        set(value) {
            dataStore.setHintsEnabledFlag(value)
        }

    var hintsCountShownFlag: Boolean
        get() = dataStore.getHintsShownFlag()
        set(value) {
            dataStore.setHintsShownFlag(value)
        }

    var musicFlag: Boolean
        get() = dataStore.getMusicFlag()
        set(value) {
            dataStore.setMusicFlag(value)
            _musicState.value = value
        }

    var soundFlag: Boolean
        get() = dataStore.getSoundFlag()
        set(value) {
            dataStore.setSoundFlag(value)
        }
}