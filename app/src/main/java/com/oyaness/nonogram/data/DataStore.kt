package com.oyaness.nonogram.data

import com.oyaness.nonogram.model.Difficulty
import com.oyaness.nonogram.model.Game
import com.oyaness.nonogram.model.Type


/**
 * @author Oya
 * @date 26.02.19
 */

interface DataStore {

    fun getGameType(): Type

    fun setGameType(type: Type)

    fun getGameDifficulty(): Difficulty

    fun setGameDifficulty(difficulty: Difficulty)

    fun getGame(): Game?

    fun setGame(game: Game)

    fun clearGame()

    fun getMusicFlag(): Boolean

    fun setMusicFlag(flag: Boolean)

    fun getSoundFlag(): Boolean

    fun setSoundFlag(flag: Boolean)

    fun getMistakeCountFlag(): Boolean

    fun setMistakeCountFlag(flag: Boolean)

    fun getHintsEnabledFlag(): Boolean

    fun setHintsEnabledFlag(flag: Boolean)

    fun getHintsShownFlag(): Boolean

    fun setHintsShownFlag(flag: Boolean)
}