package com.oyaness.nonogram.data

import com.oyaness.nonogram.model.Difficulty
import com.oyaness.nonogram.model.Game
import com.oyaness.nonogram.model.Type
import com.oyaness.nonogram.utils.SharedPreferencesManager


/**
 * @author Oya
 * @date 26.02.19
 */

class DataStoreImpl(private val sharedPreferencesManager: SharedPreferencesManager) : DataStore {

    override fun getGameType(): Type = sharedPreferencesManager.type


    override fun setGameType(type: Type) {
        sharedPreferencesManager.type = type
    }

    override fun getGameDifficulty(): Difficulty = sharedPreferencesManager.difficulty


    override fun setGameDifficulty(difficulty: Difficulty) {
        sharedPreferencesManager.difficulty = difficulty
    }

    override fun getGame(): Game? = sharedPreferencesManager.game


    override fun setGame(game: Game) {
        sharedPreferencesManager.game = game
    }

    override fun clearGame() {
        sharedPreferencesManager.clearGame()
    }

    /** Settings Flags */
    override fun getMusicFlag(): Boolean = sharedPreferencesManager.musicFlag

    override fun setMusicFlag(flag: Boolean) {
        sharedPreferencesManager.musicFlag = flag
    }

    override fun getSoundFlag(): Boolean = sharedPreferencesManager.soundFlag

    override fun setSoundFlag(flag: Boolean) {
        sharedPreferencesManager.soundFlag = flag
    }

    override fun getMistakeCountFlag(): Boolean = sharedPreferencesManager.mistakeCountFlag

    override fun setMistakeCountFlag(flag: Boolean) {
        sharedPreferencesManager.mistakeCountFlag = flag
    }

    override fun getHintsEnabledFlag(): Boolean = sharedPreferencesManager.enableHintsFlag

    override fun setHintsEnabledFlag(flag: Boolean) {
        sharedPreferencesManager.enableHintsFlag = flag
    }

    override fun getHintsShownFlag(): Boolean = sharedPreferencesManager.hintsUsedFlag

    override fun setHintsShownFlag(flag: Boolean) {
        sharedPreferencesManager.hintsUsedFlag = flag
    }
}