package com.oyaness.nonogram.base

import android.content.Intent
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.ViewGroup
import com.oyaness.nonogram.utils.MusicManager
import com.oyaness.nonogram.utils.Song
import nonogram.oyaness.nonogram.R

/**
 * @author Oya
 */

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var root: ViewGroup
    private val musicManager = MusicManager()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(setLayout())
        root = setRootView()
        initView()
    }

    override fun onPause() {
        super.onPause()
        stopMusic()
    }

    @LayoutRes
    protected abstract fun setLayout(): Int

    protected abstract fun setRootView(): ViewGroup

    protected abstract fun initView()

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransition(R.anim.slide_in_up, R.anim.no_slide)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.no_slide, R.anim.slide_out_down)
    }

    protected fun showSnackbar(message: String, finishCallback: (() -> Unit)? = null) {
        val snackbar = Snackbar.make(root, message, Snackbar.LENGTH_SHORT)
        //snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbar))
        // val tv = snackbar.view.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        //  tv.setTextColor(ContextCompat.getColor(this, android.R.color.white))
        snackbar.addCallback(object : Snackbar.Callback() {
            override fun onDismissed(snackbar: Snackbar?, event: Int) {
                super.onDismissed(snackbar, event)
                finishCallback?.let {
                    finishCallback()
                }
            }
        })
        snackbar.show()
    }

    protected fun startMusic(song: Song) {
        musicManager.start(this, song)
    }

    protected fun stopMusic() {
        musicManager.stop()
    }
}