package com.oyaness.nonogram.base

import android.arch.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()