package com.oyaness.nonogram.model


/**
 * @author Oya
 * @date 11.01.19
 */

class Grid(var difficulty: Difficulty, rows: Int, cols: Int) {

    var rowNumbers: List<String>
    var colNumbers: List<String>
    var cells: BooleanMatrix = BooleanMatrix(difficulty, rows, cols)

    init {
        this.rowNumbers = generateNumbersForRows(cells)
        this.colNumbers = generateNumbersForColumns(cells)
    }

    private fun generateNumbersForColumns(cells: BooleanMatrix): ArrayList<String> {
        val result: ArrayList<String> = ArrayList()
        for (j in 0 until cells.cols) {
            val numbersForColumn = ArrayList<Int>()
            var counter = 0
            for (i in 0 until cells.rows) {
                if (cells.grid[i][j]) {
                    counter++
                } else {
                    if (counter != 0) {
                        numbersForColumn.add(counter)
                        counter = 0
                    }
                }
            }
            if (counter != 0) {
                numbersForColumn.add(counter)
            }
            result.add(arrayToString(numbersForColumn, "\n"))
        }
        return result
    }

    private fun generateNumbersForRows(cells: BooleanMatrix): ArrayList<String> {
        val result: ArrayList<String> = ArrayList()
        for (i in 0 until cells.rows) {
            val numbersForRow = ArrayList<Int>()
            var counter = 0
            for (j in 0 until cells.cols) {
                if (cells.grid[i][j]) {
                    counter++
                } else {
                    if (counter != 0) {
                        numbersForRow.add(counter)
                        counter = 0
                    }
                }
            }
            if (counter != 0) {
                numbersForRow.add(counter)
            }
            result.add(arrayToString(numbersForRow, ", "))
        }
        return result
    }

    private fun arrayToString(list: List<Int>, separator: String): String {
        return if (list.isEmpty()) {
            "X"
        } else {
            list.joinToString(separator = separator)
        }
    }
}
