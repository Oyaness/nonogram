package com.oyaness.nonogram.model

import kotlin.random.Random


/**
 * @author Oya
 * @date 11.01.19
 */

class BooleanMatrix {
    val rows: Int
    val cols: Int
    var grid: Array<BooleanArray>

    constructor(rows: Int, cols: Int) {
        this.rows = rows
        this.cols = cols
        this.grid = Array(rows) { BooleanArray(cols) { false } }
    }

    constructor(difficulty: Difficulty, rows: Int, cols: Int) {
        this.rows = rows
        this.cols = cols
        this.grid = Array(rows) { BooleanArray(cols) { false } }
        val activeCellCount = getCellCountForDifficulty(rows, cols, difficulty)
        fillGrid(activeCellCount)
    }


    private fun fillGrid(activeCellCount: Int) {
        var row: Int
        var col: Int
        for (i in 0 until activeCellCount) {
            do {
                col = Random.nextInt(cols)
                row = Random.nextInt(rows)
            } while (grid[row][col])

            grid[row][col] = true
        }
    }

    private fun getCellCountForDifficulty(rowCount: Int, colCount: Int, difficulty: Difficulty): Int {
        val gridCellCount = rowCount * colCount
        return when (difficulty) {
            Difficulty.Easy -> {
                gridCellCount * 90 / 100
            }
            Difficulty.Medium -> {
                gridCellCount * 70 / 100
            }
            Difficulty.Hard -> {
                gridCellCount * 50 / 100
            }
        }
    }
}