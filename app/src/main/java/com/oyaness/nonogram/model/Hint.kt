package com.oyaness.nonogram.model


/**
 * @author Oya
 * @date 27.03.19
 */
data class Hint(val x: Int, val y: Int, val checked: Boolean)