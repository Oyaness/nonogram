package com.oyaness.nonogram.model

import kotlin.math.roundToInt

/**
 * @author Oya
 * @date 11.01.19
 */

class Game(val gameType: Type, difficulty: Difficulty, height: Int, width: Int, cellSize: Float, cellMargin: Float) {
    val grid: Grid
    var buffer: Long = 0
    var guessedCells: BooleanMatrix

    init {
        val rows = calculateRowCount(height, cellSize, cellMargin)
        val cols = calculateColCount(width, cellSize, cellMargin)
        this.grid = Grid(difficulty, rows, cols)
        this.guessedCells = BooleanMatrix(rows, cols)
    }

    private fun calculateRowCount(height: Int, cellSize: Float, cellMargin: Float): Int {
        return ((0.7f * height) / (cellSize + cellMargin)).roundToInt()
    }

    private fun calculateColCount(width: Int, cellSize: Float, cellMargin: Float): Int {
        return ((0.85f * width) / (cellSize + cellMargin)).roundToInt()
    }

}