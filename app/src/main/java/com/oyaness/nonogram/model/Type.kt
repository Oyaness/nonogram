package com.oyaness.nonogram.model

import nonogram.oyaness.nonogram.R


/**
 * @author Oya
 * @date 27.02.19
 */

enum class Type(val state:IntArray) {
    Regular(intArrayOf(R.attr.state_regular, -R.attr.state_timer)),
    Timer(intArrayOf(-R.attr.state_regular, R.attr.state_timer))
}