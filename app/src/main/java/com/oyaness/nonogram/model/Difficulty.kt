package com.oyaness.nonogram.model

import nonogram.oyaness.nonogram.R


/**
 * @author Oya
 * @date 27.02.19
 */

enum class Difficulty(val state:IntArray) {
    Easy(intArrayOf(R.attr.state_easy, -R.attr.state_medium, -R.attr.state_hard)),
    Medium(intArrayOf(-R.attr.state_easy, R.attr.state_medium, -R.attr.state_hard)),
    Hard(intArrayOf(-R.attr.state_easy, -R.attr.state_medium, R.attr.state_hard))
}